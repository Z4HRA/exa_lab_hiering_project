from flask import Flask, jsonify, request
import api.models as model

app = Flask(__name__)


#  @app.route('/hashtag/api/v1.0/top_repetitive', methods=['GET'])

@app.route('/', methods=['GET'])
def hashtag_info():
    query_string = request.args["hashtag"]
    hashtag = model.Hashtag(query_string)
    top_ten_repetitive = hashtag.repetitive_hashtag(10)
    top_ten_user = hashtag.influencer(10)

    response = {"repetitive_hashtags": top_ten_repetitive,
                "influenc_users": top_ten_user}

    return jsonify(response)
