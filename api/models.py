from neo4j import GraphDatabase
import neo4j
import os
import re
import json

user = os.environ.get("NEO4J_USER")
password = os.environ.get("PASSWORD")
neo4j_uri = os.environ.get("URI")
gdb = GraphDatabase()
driver = gdb.driver(neo4j_uri, auth=(user, password))


def preprocess(path):
    filepath = path[:path.rfind(".")]
    with open(path, "r", encoding='utf8') as source, open(filepath + "_p.json", "w", encoding='utf8') as result:
        for line in source:
            data = re.sub(r'ObjectId\s*\(\s*\"(\S+)\"\s*\)', r'"\1"', line)
            data = re.sub(r'NumberLong\s*\(\s*\"*(\S+[^\"])\"*\s*\)', r'"\1"', data)
            data = re.sub("_id", "id", data)
            # result.write(data)
            data = json.loads(data.encode('utf8'))
            json.dump(data, result, ensure_ascii=False)


def import_data(path):
    database_name = os.environ.get("DATABASE_NAME")
    session = driver.session(database=database_name)
    query_load = "CALL apoc.load.json('{0}') YIELD value AS data UNWIND data as tw return tw".format(path)
    query_create = "MERGE (tweet:Tweet {id: tw.TweetId}) " \
                   "ON CREATE SET tweet.text = tw.Text " \
                   "MERGE (user:User {id: tw.TwitterUserEntityModel.id}) " \
                   "ON CREATE SET user.name = tw.TwitterUserEntityModel.ScreenName, " \
                   "user.follower = tw.TwitterUserEntityModel.FollowersCount," \
                   "user.friends = tw.TwitterUserEntityModel.FriendsCount," \
                   "user.fav = tw.TwitterUserEntityModel.FovouritesCount " \
                   "MERGE (user)-[:POSTED]->(tweet) " \
                   "FOREACH (h IN tw.Entities.Hashtag | MERGE (hashtag:Hashtag {id:toLower(h.Text)}) " \
                   "MERGE (hashtag)-[:IN]->(tweet));"
    query_iter = 'CALL apoc.periodic.iterate("' + query_load + '","' + query_create + '", {batchSize: 1000, iterateList: true});'
    query = neo4j.Query(query_iter)
    result = session.run(query)
    session.close()
    return result


class Hashtag:
    def __init__(self, text):
        self.text = text

    def repetitive_hashtag(self, n):
        database_name = os.environ.get("DATABASE_NAME")
        with driver.session(database=database_name) as session:
            query_str = 'MATCH (h1: Hashtag)-[:IN]->(t:Tweet)<-[:IN]-(h2: Hashtag)\r\n' + \
                        "WHERE h1.id = toLower(rTrim('" + self.text + "'))\r\n" + \
                        'RETURN h2.id as text, count(h2.id) AS n\r\n' + \
                        'ORDER BY n DESC\r\n' + \
                        'LIMIT ' + str(n) + ';'
            result = session.run(neo4j.Query(query_str))
            result = [record["text"] for record in result]

        return result

    def influencer(self, n):
        database_name = os.environ.get("DATABASE_NAME")
        with driver.session(database=database_name) as session:
            query_str = 'MATCH (h:Hashtag)-[:IN]->(t:Tweet)<-[:POSTED]-(u:User)\r\n' + \
                        'WHERE h.id = toLower(rTrim("' + self.text + '"))\r\n' + \
                        'RETURN distinct u.name as name, u.follower\r\n' + \
                        'ORDER BY u.follower DESC\r\n' + \
                        'LIMIT ' + str(n) + ";"
            result = session.run(neo4j.Query(query_str))
            result = [record["name"] for record in result]
        return result
